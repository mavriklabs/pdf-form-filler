package co.mavrik;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This example will take a PDF document and print all the fields from the file.
 *
 * @author Adi Kancherla
 */
public class EquitasFormFiller {

    private static String TEMPLATE_FOLDER = "assets" + File.separator + "templates";
    private static String INPUT_TEMPLATE = "equitas-template.pdf";
    private static String FILLED_FORMS_FOLDER = "filled-forms";
    private static List<PDField> pdFields = null;
    private static String template = TEMPLATE_FOLDER + File.separator + INPUT_TEMPLATE;

    /**
     * @param args command line arguments
     * @throws IOException If there is an error importing the FDF document.
     */
    public static void main(String[] args) throws IOException {
        PDDocument pdf = null;
        try {
            pdf = PDDocument.load(new File(template));
            pdFields = getFields(pdf);
            if (pdFields.isEmpty()) {
                throw new IOException("PDFields is empty");
            } else {
                //set fields
                Map<String, String> fields = getData();
                setFields(pdf, fields);
                String outputFileName = fields.get("fullName") + "-" + fields.get("email") + ".pdf";
                String outputPath = FILLED_FORMS_FOLDER + File.separator + outputFileName;
                pdf.save(new File(outputPath));
            }
        } finally {
            if (pdf != null) {
                pdf.close();
            }
        }
    }

    private static Map<String, String> getData() {
        Map<String, String> fields = new HashMap<>();
        fields.put("fullName", "Adi Kancherla");
        fields.put("name", "Adi");
        fields.put("nre", "Yes");
        fields.put("nro", "Yes");
        fields.put("email", "a@mavrik.co");
        return fields;
    }

    /**
     * This will get all the fields from the document.
     *
     * @param pdfDocument The PDF to get the fields from.
     * @throws IOException If there is an error getting the fields.
     */
    private static List<PDField> getFields(PDDocument pdfDocument) throws IOException {
        PDDocumentCatalog docCatalog = pdfDocument.getDocumentCatalog();
        PDAcroForm acroForm = docCatalog.getAcroForm();
        List<PDField> fields = acroForm.getFields();

        System.out.println(fields.size() + " top-level fields were found on the form");

        for (PDField field : fields) {
            String fieldName = field.getPartialName();
            String className = field.getClass().getName();
            String[] classNameTruncArr = className.split("\\.");
            String fieldType = classNameTruncArr[classNameTruncArr.length - 1];
            System.out.println(fieldName + "=" + field.getValueAsString() + " type=" + fieldType);
        }
        return fields;
    }

    private static void setFields(PDDocument pdfDocument, Map<String, String> fields) throws IOException {
        PDDocumentCatalog docCatalog = pdfDocument.getDocumentCatalog();
        PDAcroForm acroForm = docCatalog.getAcroForm();
        for (Map.Entry<String, String> field : fields.entrySet()) {
            String fieldName = field.getKey();
            String fieldValue = field.getValue();
            PDField pdField = acroForm.getField(fieldName);
            setField(pdField, fieldValue);
        }
    }

    /**
     * This will set a single field in the document.
     *
     * @param pdField The PDF to set the field in.
     * @param value   The new value of the field.
     * @throws IOException If there is an error setting the field.
     */
    private static void setField(PDField pdField, String value) throws IOException {
        if (pdField != null) {
            if (pdField instanceof PDCheckBox) {
                if (value.equalsIgnoreCase("yes")) {
                    ((PDCheckBox)pdField).check();
                } else {
                    ((PDCheckBox)pdField).unCheck();
                }
            } else {
                pdField.setValue(value);
            }
        } else {
            System.err.println("No field found with name:" + pdField.getPartialName());
        }
    }
}